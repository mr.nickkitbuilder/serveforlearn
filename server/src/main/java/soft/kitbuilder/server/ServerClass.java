package soft.kitbuilder.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

import serverAdapter.EventInterface;
import serverAdapter.ServerAdapter;

public class ServerClass {
    static ServerSocket server;
    static ArrayList<ServerAdapter> socks;
    static ServerAdapter sa;
    public static void main(String args[]) throws IOException {
        server = new ServerSocket(1002);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    sa = new ServerAdapter();
                    sa.setOnFullEventListener(new EventInterface() {
                        @Override
                        public void fullArr() {
                            System.out.println(sa.getName());
                        }
                    });
                    sa = (ServerAdapter) server.accept();
                    socks.add(sa);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }
}
