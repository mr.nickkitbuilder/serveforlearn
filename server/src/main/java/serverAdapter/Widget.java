package serverAdapter;

public class Widget {
    public int id;
    public String a;
    public String b;

    public Widget(int id, String a, String b) {

        this.id = id;
        this.a = a;
        this.b = b;
    }

    public Widget() {
        a = "not initialised";
        b = "not initialised";
        id = 1000;
    }
}
