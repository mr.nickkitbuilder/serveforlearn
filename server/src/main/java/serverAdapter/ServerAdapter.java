package serverAdapter;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class ServerAdapter extends Socket{
    private DataInputStream dis;
    private DataOutputStream dos;
    private String name;
    private String des;
    private ArrayList<Widget> low;
    private Widget old;
    private int size;
    private EventInterface eventInterface;

    public void setOnFullEventListener(EventInterface eventInterface){
        this.eventInterface = eventInterface;
    }

    public String getName() {
        return name;
    }

    public String getDes() {
        return des;
    }

    public ArrayList<Widget> getLow() {
        return low;
    }

    public ServerAdapter() throws IOException {
        dis = new DataInputStream(getInputStream());
        dos = new DataOutputStream(getOutputStream());
        old = new Widget();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        String data = dis.readUTF().toString();
                        switch(data.charAt(0)){
                            case 'n':name = data.substring(1);
                                break;
                            case 'd':des = data.substring(1);
                                break;
                            case 's':size = Integer.parseInt(data.substring(1));
                                break;
                            case 'a':
                                if(size !=0){
                                    old.a = data.substring(2);
                                }
                                break;
                            case 'b':
                                if(size !=0){
                                    old.b = data.substring(2);
                                }
                                break;
                            case 'i':
                                if(size !=0){
                                    old.id = Integer.parseInt(data.substring(2));
                                }
                                break;

                        }
                        if(old.a != "not initialised" &&  old.b != "not initialised" && old.id != 1000){
                            low.add(old);
                            old = new Widget();
                        }
                        if(low.size() == size && size != 0){
                            eventInterface.fullArr();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
